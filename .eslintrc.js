module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true
    },
    "extends":[
        "eslint:recommended",
        "plugin:react/recommended"
      ],
    
    "parserOptions": {
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true,
            "jsx": true
        },
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
    "react/jsx-uses-react": "error",
    "react/jsx-uses-vars": "error",
    "react/react-in-jsx-scope": "off",
        "indent": 0,

        "linebreak-style": 0,
        
        "quotes": [2, "single", { "avoidEscape": true }],
        "semi": [
            "error",
            "always"
        ]
    }
};